let s:addresses = systemlist("khard email -p --remove-first-line
            \ | sed 's/\\(.*\\)\\t\\(.*\\)\\t\\(.*\\)/\\2 <\\1>/'")

function! s:complete_khard(context)
    call ncm2#complete(a:context, a:context.startccol, s:addresses)
endfunction

let s:khard_source = {
    \ 'name': 'Khard',
    \ 'mark': 'khard',
    \ 'scope': ['mail'],
    \ 'priority': 9,
    \ 'complete_length': -1,
    \ 'complete_pattern': '^(To|Cc|Bcc): .*',
    \ 'on_complete': function('s:complete_khard')}

call ncm2#register_source(s:khard_source)
